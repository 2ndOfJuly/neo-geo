import Wireframe from "../layouts/Wireframe";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
    return (
        <Wireframe>
            <Component {...pageProps} />
        </Wireframe>
    );
}

export default MyApp;
