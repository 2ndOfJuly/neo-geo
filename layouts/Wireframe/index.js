import Head from "next/head";
import styles from "./Wireframe.module.css";

export default function Wireframe({ children }) {
    return (
        <div className={styles.container}>
            <Head>
                <title>Neo-Geo</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main className={styles.main}>{children}</main>

            <footer className={styles.footer}></footer>
        </div>
    );
}
